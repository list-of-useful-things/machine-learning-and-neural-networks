### Libraries
* [RubixML](https://github.com/RubixML)
* [Neural networks in JavaScript](https://scrimba.com/g/gneuralnetworks)
* [PYTHORCH](http://pytorch.org/)
* [Caffe](http://caffe.berkeleyvision.org/)
* [Caffe2](https://caffe2.ai/)
* [GENESIS](http://genesis-sim.org/)
* [TensorFlow](https://www.tensorflow.org/)
* [Keras](https://keras.io/)
* [Torch](http://torch.ch/)
* [Apache MXNet](https://mxnet.apache.org/)
* [MS CNTK](https://github.com/Microsoft/CNTK)
* [MS CNTK](https://www.microsoft.com/en-us/cognitive-toolkit/)
* [WEKA](https://www.cs.waikato.ac.nz/ml/weka/)
* [caret](http://topepo.github.io/caret/index.html)
* [DEAP](https://github.com/DEAP/deap)
* [OTHER - face detection](https://github.com/ShiqiYu/libfacedetection)
* [cortex - Machine learning infrastructure for developers](https://github.com/cortexlabs/cortex)

### Articles
* [Recurrent Neural Network](https://en.wikipedia.org/wiki/Recurrent_neural_network)
* [How to solve 90% of NLP problems: a step-by-step guide](https://blog.insightdatascience.com/how-to-solve-90-of-nlp-problems-a-step-by-step-guide-fda605278e4e)
* [Comparison of deep learning software](https://en.wikipedia.org/wiki/Comparison_of_deep_learning_software)
* [How to Develop LSTM Models for Multi-Step Time Series Forecasting of Household Power Consumption](https://machinelearningmastery.com/how-to-develop-lstm-models-for-multi-step-time-series-forecasting-of-household-power-consumption/?__s=d9suuqk68mperdkszern)
* [Deep Learning for Time Series Forecasting](https://machinelearningmastery.com/deep-learning-for-time-series-forecasting/?__s=wyup4fgzzitakicgnrt8)
* [Machine Learning for Humans, Part 5: Reinforcement Learning](https://medium.com/machine-learning-for-humans/reinforcement-learning-6eacf258b265)
* [Reinforcement Learning Explained](https://www.edx.org/course/reinforcement-learning-explained)
* [An introduction to Reinforcement Learning](https://medium.freecodecamp.org/an-introduction-to-reinforcement-learning-4339519de419)
* [A Beginner’s Guide to Deep Reinforcement Learning](https://deeplearning4j.org/deepreinforcementlearning)
* [A Beginner’s Guide to Neural Networks in Python and SciKit Learn 0.18](https://www.springboard.com/blog/beginners-guide-neural-network-in-python-scikit-learn-0-18/)
* [Practical Deep Learning for Coders, v3](https://course.fast.ai/) | [Practical Deep Learning for Coders 2019](https://www.fast.ai/2019/01/24/course-v3/)
* [Machine Learning for Everyone](https://vas3k.com/blog/machine_learning/)
* [Deep Dive into Computer Vision with Neural Networks: Part 1](https://www.alibabacloud.com/blog/deep-dive-into-computer-vision-with-neural-networks-%E2%80%93-part-1_593897)
* [Deep Dive Into Computer Vision With Neural Networks: Part 2](https://dzone.com/articles/deep-dive-into-computer-vision-with-neural-network?fromrel=true)
* [From R-CNN to Faster R-CNN: The Evolution of Object Detection Technology](https://dzone.com/articles/from-r-cnn-to-faster-r-cnn-the-evolution-of-object?fromrel=true)
* [Can Deep Networks Learn Invariants?](https://dzone.com/articles/can-deep-networks-learn-invariants?fromrel=true)
* [The Complete Guide to Learn Machine Learning from Scratch](https://medium.com/@itexpert/the-complete-guide-to-learn-machine-learning-from-scratch-46a6734acda6)
* [Basic concepts of machine learning](https://medium.com/@mrkardostamas/basic-concepts-of-machine-learning-d9e64c64aa94)
* [Machine Learning for Beginners: An Introduction to Neural Networks](https://victorzhou.com/blog/intro-to-neural-networks/)
* [Introduction to K-Means Clustering in Python with scikit-learn](https://blog.floydhub.com/introduction-to-k-means-clustering-in-python-with-scikit-learn/)
* [10 Amazing Articles On Python Programming And Machine Learning](https://hackernoon.com/10-great-articles-on-python-development-6f54dd38437f)
* [4 TYPES OF MACHINE LEARNING ALGORITHMS](https://theappsolutions.com/blog/development/machine-learning-algorithm-types/)
* [Weight Agnostic Neural Networks](https://weightagnostic.github.io/)

##### Golang
* [Deep learning in Go](https://medium.com/@hackintoshrao/deep-learning-in-go-f13e586f7d8a)

### Tutorials
* [Introduction to TensorFlow](https://dzone.com/refcardz/introduction-to-tensorflow?chapter=1)
* [Introduction to Neural Networks](https://github.com/simplefractal/learnathon-neural-networks/blob/master/readme.md)
* [Applied Deep Learning with PyTorch](https://morioh.com/p/433f0ddf1da6/applied-deep-learning-with-pytorch-full-course)
* [A Complete Machine Learning Project Walk-Through in Python (Part One): Putting the machine learning pieces together](https://morioh.com/p/b56ae6b04ffc/a-complete-machine-learning-project-walk-through-in-python)

### Related...
* [Computer Vision Using OpenCV](https://dzone.com/articles/opencv-python-tutorial-computer-vision-using-openc)
 
### Videos
* [MarI/O - Machine Learning for Video Games](https://www.youtube.com/watch?v=qv6UVOQ0F44)
* [A more interesting self-driving neural network model - Python plays GTA p.13](https://www.youtube.com/watch?v=nWJZ4w0HKz8)
* [Roadmap: How to Learn Machine Learning in 6 Months](https://www.youtube.com/watch?v=MOdlp1d0PNA)
* [BASICS - How Neural Networks Work - Simply Explained](https://www.youtube.com/watch?v=GqfzCTpCODE)
* [BASICS - Machine Learning Tutorial Part 1 | Machine Learning For Beginners](https://www.youtube.com/watch?v=E3l_aeGjkeI)

### Books
* [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/chap1.html)
* [Dive into Deep Learning](https://d2l.ai/index.html)
* [Dive into Deep Learning GitHub](https://github.com/d2l-ai/d2l-en)
* [The AI Programmer's Booshelf](alumni.media.mit.edu/~jorkin/aibooks.html)

### Other
* [Robocode / programming game](https://robocode.sourceforge.io/)

### Services / Platforms
* Google Cloud Platform
* Amazon Webservices
* Microsoft Cognitive Services
* IBM Watson
* [Open Source Deep Learning Server](https://www.deepdetect.com/)
* Azure ML
* https://www.customvision.ai/